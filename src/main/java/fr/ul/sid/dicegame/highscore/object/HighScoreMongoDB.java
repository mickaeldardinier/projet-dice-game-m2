package fr.ul.sid.dicegame.highscore.object;

import java.util.logging.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import fr.ul.sid.dicegame.highscore.Entry;
import fr.ul.sid.dicegame.highscore.connexionDatabase.ConnexionMongoDB;

public class HighScoreMongoDB implements HighScore {

	private Entry hs;

	private static Logger logger = Logger.getLogger(HighScoreMySQL.class.getName());

	@Override
	public void load() {
		DB db = ConnexionMongoDB.getInstance();
		DBCollection collection = db.getCollection("highscore");
		if (collection.count() > 0) {
			DBObject query = new BasicDBObject("_id", "highscore");
			DBCursor cursor = collection.find(query);
			if (cursor.count() > 0) {
				DBObject dbHighscore = cursor.one();

				String nom = (String) dbHighscore.get("nom");
				int score = (int) dbHighscore.get("score");
				hs = new Entry(nom, score);
				logger.info("R�cup�ration r�ussi.");
			}
			else {
				logger.info("Aucun Highscore");
			}
		} else {
			logger.info("Aucun Highscore");
		}
	}

	@Override
	public void save() {
		DB db = ConnexionMongoDB.getInstance();
		DBCollection collection = db.getCollection("highscore");
		DBObject query = new BasicDBObject("_id", "highscore");
		DBCursor cursor = collection.find(query);
		if (cursor.count() > 0) {
			DBObject dbHighscore = new BasicDBObject("_id", "highscore").append("nom", hs.getNom()).append("score",
					hs.getScore());
			collection.update(cursor.one(), dbHighscore);
			logger.info("Actualisation des informations");
		} else {
			DBObject dbHighscore = new BasicDBObject("_id", "highscore").append("nom", hs.getNom()).append("score",
					hs.getScore());
			collection.insert(dbHighscore);
			logger.info("Enregistrement r�ussi.");
		}
	}

	@Override
	public String whoTheHellAreYou() {
		System.out.println("MongoDB");
		return "MongoDB";
	}

	@Override
	public void add(String nom, int score) {
		this.hs = new Entry(nom, score);
	}

	public Entry getHs() {
		return hs;
	}
}
