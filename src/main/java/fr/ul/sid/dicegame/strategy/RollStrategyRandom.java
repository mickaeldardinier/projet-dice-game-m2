package fr.ul.sid.dicegame.strategy;
import java.util.Random;

public class RollStrategyRandom implements RollStrategy{

	@Override
	public int roll() {
		Random r = new Random();
		return r.nextInt(6)+1;
	}
	
	public String toString() {
		return "Strat�gie 'al�atoire'";
	}

}
