package fr.ul.sid.dicegame.interfaceApplication;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ApplicationDiceGame extends Application {
	
	private static final Logger logger = Logger.getLogger(ApplicationDiceGame.class.getName());

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("/interfaceDiceGame.fxml"));
		} catch (IOException e) {
			logger.severe(e.getMessage());
		}
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
