package fr.ul.sid.dicegame.highscore.kit;

import fr.ul.sid.dicegame.highscore.object.HighScore;
import fr.ul.sid.dicegame.highscore.object.HighScoreSr;

public class SrKit implements PersistKit {

	@Override
	public HighScore makeKit() {
		return new HighScoreSr();
	}
}