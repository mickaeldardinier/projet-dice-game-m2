package fr.ul.sid.dicegame.highscore.object;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Logger;

import fr.ul.sid.dicegame.highscore.Entry;

public class HighScoreSr implements HighScore {

	private String filename = "src/main/resources/HighScoreFile.txt";
	
	private Entry hs;

	private static Logger logger = Logger.getLogger(HighScoreSr.class.getName());

	@Override
	public void load() {
		try {
			ObjectInputStream input = new ObjectInputStream(new FileInputStream(filename));
			this.hs = (Entry) input.readObject();
			input.close();
		} catch (IOException | ClassNotFoundException e) {
			logger.severe(e.getMessage());
		}
	}

	@Override
	public void save() {
		try {
			ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(filename));
			output.writeObject(this.hs);
			output.close();
		} catch (IOException e) {
			logger.severe(e.getMessage());
		}
	}

	@Override
	public String whoTheHellAreYou() {
		System.out.println("Serialisable");
		return "Serialisable";
	}

	@Override
	public void add(String nom, int score) {
		this.hs = new Entry(nom, score);	
	}

	public Entry getHs() {
		return hs;
	}
}