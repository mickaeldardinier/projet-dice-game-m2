package fr.ul.sid.dicegame;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class Classement {
	
	public static HashMap<Player, Integer> ordonnerClassement(List<Player> players){
		players.sort(new Comparator<Player>() {
			@Override
			public int compare(Player p1, Player p2) {
				if (p1.getScore() < p2.getScore()) {
					return 1;
				} else if (p1.getScore() > p2.getScore()) {
					return -1;
				}
				return 0;
			}
		});

		HashMap<Player, Integer> classement = new HashMap<Player, Integer>();
		int rang = 1;
		for(int i = 0; i< players.size();i++) {
			Player p = players.get(i);
			classement.put(p,rang);
			if(players.size() > i+1) {
				if(players.get(i+1).getScore() < p.getScore()) {
					rang++;
				}
			}
		}

		return classement;
	}
	
//	public static String formater(HashMap<Player, Integer> classement) {
//		List keys = new ArrayList(classement.keySet());
//		String resultats = "Classement : \n";
//		for (int i=0; i<keys.size(); i++) {
//			resultats+= keys.get(i) + "\n";
//		}
//		
//		return resultats;
//	}
}
