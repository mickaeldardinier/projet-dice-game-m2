package fr.ul.sid.dicegame.highscore.kit;

import fr.ul.sid.dicegame.highscore.object.HighScore;
import fr.ul.sid.dicegame.highscore.object.HighScoreMongoDB;

public class MongoDBKit implements PersistKit {

	@Override
	public HighScore makeKit() {
		return new HighScoreMongoDB();
	}
}