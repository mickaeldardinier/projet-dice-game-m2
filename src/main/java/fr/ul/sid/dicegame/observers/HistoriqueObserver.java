package fr.ul.sid.dicegame.observers;

import java.util.Observable;
import java.util.Observer;

import fr.ul.sid.dicegame.historique.CareTaker;
import javafx.scene.control.TextArea;

public class HistoriqueObserver implements Observer {

	private static HistoriqueObserver instance;

	private TextArea historique;

	public static HistoriqueObserver getInstance() {
		if (instance == null) {
			instance = new HistoriqueObserver();
		}
		return instance;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		historique.setText(((CareTaker) arg1).toString());
	}

	public TextArea getHistorique() {
		return historique;
	}

	public void setHistorique(TextArea historique) {
		this.historique = historique;
	}

}
