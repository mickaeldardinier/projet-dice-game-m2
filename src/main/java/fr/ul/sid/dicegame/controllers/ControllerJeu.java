package fr.ul.sid.dicegame.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import fr.ul.sid.dicegame.DiceGame;
import fr.ul.sid.dicegame.Die;
import fr.ul.sid.dicegame.strategy.RollStrategy;
import fr.ul.sid.dicegame.strategy.RollStrategy1or6;
import fr.ul.sid.dicegame.strategy.RollStrategyOnly3;
import fr.ul.sid.dicegame.strategy.RollStrategyRandom;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class ControllerJeu implements Initializable {

	@FXML
	private Label numeroTour, aToiDeJouer, resultatLancer, points, ajouterJoueursErreur, strategieLancer,
			strategieChoisie, erreurStrategie;

	@FXML
	private Button lancerPartie, lancerDes, random, trois, unOuSix, arreter;

	@FXML
	private TextArea score;

	private RollStrategy rs = null;

	private DiceGame diceGame = DiceGame.getInstance();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		lancerPartie.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				if (diceGame.getPlayers().isEmpty() || diceGame.isFinPartie())
					ajouterJoueursErreur.setVisible(true);
				else {
					ajouterJoueursErreur.setVisible(false);
					lancerPartie.setDisable(true);
					numeroTour.setText("Tour num�ro " + diceGame.getCurrentTour());
					numeroTour.setVisible(true);
					aToiDeJouer.setText("A toi de jouer " + diceGame.getPlayers().get(0).getName());
					aToiDeJouer.setVisible(true);
					lancerDes.setVisible(true);
					strategieLancer.setVisible(true);
					random.setVisible(true);
					trois.setVisible(true);
					unOuSix.setVisible(true);
					afficherScore();
					score.setVisible(true);
					lancerDes.setDisable(false);
					random.setDisable(false);
					trois.setDisable(false);
					unOuSix.setDisable(false);
					arreter.setVisible(true);
					arreter.setDisable(false);
				}
			}
		});

		lancerDes.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (!diceGame.isFinPartie() && rs != null) {

					erreurStrategie.setVisible(false);
					strategieChoisie.setVisible(false);

					ArrayList<Integer> resultats = diceGame.tourJoueur(new Die(), new Die(),
							diceGame.getCurrentPlayer(), rs);

					resultatLancer.setText(resultats.get(0) + "," + resultats.get(1));

					resultatLancer.setVisible(true);

					points.setText("Le " + diceGame.getCurrentPlayer().getName() + " a marqu� " + resultats.get(2)
							+ " points.");

					points.setVisible(true);

					numeroTour.setText("Tour num�ro " + diceGame.getCurrentTour());

					aToiDeJouer.setText("A toi de jouer " + diceGame.getNextPlayer().getName());

					diceGame.gererTour();

					rs = null;

					afficherScore();

					if (diceGame.isFinPartie()) {
						resetPartie();
					}

					// todo observer pour le score, highscore

				} else if (rs == null) {
					erreurStrategie.setVisible(true);
				}
			}
		});

		random.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				rs = new RollStrategyRandom();
				erreurStrategie.setVisible(false);
				strategieChoisie.setVisible(true);
			}

		});

		trois.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				rs = new RollStrategyOnly3();
				erreurStrategie.setVisible(false);
				strategieChoisie.setVisible(true);
			}
		});

		unOuSix.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				rs = new RollStrategy1or6();
				erreurStrategie.setVisible(false);
				strategieChoisie.setVisible(true);
			}
		});

		arreter.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				resetPartie();
			}
		});
	}

	public void afficherScore() {
		String texteScore = "Score : \n";
		for (int i = 0; i < diceGame.getPlayers().size(); i++) {
			texteScore += diceGame.getPlayers().get(i).getName() + " : " + diceGame.getPlayers().get(i).getScore()
					+ "\n";
		}
		score.setText(texteScore);
	}

	public void resetPartie() {
		lancerDes.setDisable(true);
		random.setDisable(true);
		trois.setDisable(true);
		unOuSix.setDisable(true);
		aToiDeJouer.setVisible(false);
		numeroTour.setVisible(false);
		resultatLancer.setVisible(false);
		points.setVisible(false);
		lancerPartie.setDisable(false);
		DiceGame.reset();
		diceGame = DiceGame.getInstance();
		arreter.setDisable(true);
	}
}
