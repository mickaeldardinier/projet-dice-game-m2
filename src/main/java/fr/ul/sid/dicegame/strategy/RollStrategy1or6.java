package fr.ul.sid.dicegame.strategy;

import java.util.Random;

public class RollStrategy1or6 implements RollStrategy {

	@Override
	public int roll() {
		Random r = new Random();
		int res = r.nextInt(2);
		if(res == 0) {
			return 1;
		}
		return 6;
	}
	
	public String toString() {
		return "stratégie 'un ou six'";
	}

}
