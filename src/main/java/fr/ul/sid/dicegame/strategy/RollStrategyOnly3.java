package fr.ul.sid.dicegame.strategy;

public class RollStrategyOnly3 implements RollStrategy{

	@Override
	public int roll() { 
		return 3;				
	}
	
	public String toString() {
		return "Stratégie 'seulement trois'";
	}
	
}