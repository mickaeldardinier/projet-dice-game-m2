package fr.ul.sid.dicegame.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import fr.ul.sid.dicegame.DiceGame;
import fr.ul.sid.dicegame.Player;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ControllerConfig implements Initializable {
	
	@FXML
	private Label joueursAjoutes, erreur, erreurImpoConfig;
	
	@FXML
	private TextField nbJoueurs;

	@FXML
	private Button valider;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		valider.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				if(DiceGame.getInstance().getPlayers().isEmpty()) {
					if(!nbJoueursCorrect(nbJoueurs.getText())) {
						erreurImpoConfig.setVisible(false);
						joueursAjoutes.setVisible(false);
						erreur.setVisible(true);
					}
					else {
						List<Player> players = new ArrayList<Player>();
						for(int i=1; i<=Integer.parseInt(nbJoueurs.getText()); i++) {
							Player player = new Player("Joueur " + i);
							players.add(player);
						}
						DiceGame.getInstance().setPlayers(players);
						erreurImpoConfig.setVisible(false);
						erreur.setVisible(false);
						joueursAjoutes.setVisible(true);
						DiceGame.getInstance().setFinPartie(false);
					}
				} else {
					erreurImpoConfig.setVisible(true);
					joueursAjoutes.setVisible(false);
					erreur.setVisible(false);
				}
			}
		});
	}
	
	public static boolean nbJoueursCorrect(String input) {
	    try {
	    	int nb = Integer.parseInt(input);
	    	if(nb>=2 && nb<=10)
	    		return true;
	    	return false;
	    } catch (final NumberFormatException e) {
	        return false;
	    }
	}

}
