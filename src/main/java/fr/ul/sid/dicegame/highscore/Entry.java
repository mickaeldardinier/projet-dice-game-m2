package fr.ul.sid.dicegame.highscore;
import java.io.Serializable;

public class Entry implements Serializable {

	private String nom;
	
	private int score;
	
	public Entry(String nom, int score) {
		setNom(nom);
		setScore(score);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "HighScore : nom=" + nom + ", score=" + score;
	}
}
