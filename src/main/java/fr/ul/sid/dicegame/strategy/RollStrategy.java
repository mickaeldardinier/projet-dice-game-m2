package fr.ul.sid.dicegame.strategy;

public interface RollStrategy {

	public int roll();

}
