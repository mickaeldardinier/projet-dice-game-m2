package fr.ul.sid.dicegame.highscore.connexionDatabase;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class ConnexionMongoDB {

	private static DB connexion;

	private static final Logger logger = Logger.getLogger(ConnexionMariaDB.class.getName());

	private ConnexionMongoDB() {
			try {
				connexion = new MongoClient(new MongoClientURI("mongodb://localhost:27017")).getDB("dicegame");
			} catch (UnknownHostException e) {
				logger.severe(e.getMessage());
			}
			logger.info("Base de donn�es pr�te � l'emploi.");

	}

	// M�thode qui va nous retourner notre instance et la cr�er si elle n'existe
	// pas
	public static DB getInstance() {
		if (connexion == null) {
			new ConnexionMongoDB();
		}
		return connexion;
	}

}
