package fr.ul.sid.dicegame.highscore.kit;

import fr.ul.sid.dicegame.highscore.object.HighScore;
import fr.ul.sid.dicegame.highscore.object.HighScoreMariaDB;

public class MariaDBKit implements PersistKit {

	@Override
	public HighScore makeKit() {
		return new HighScoreMariaDB();
	}
}