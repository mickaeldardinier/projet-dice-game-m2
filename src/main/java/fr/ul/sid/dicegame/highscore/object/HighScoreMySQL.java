package fr.ul.sid.dicegame.highscore.object;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import fr.ul.sid.dicegame.highscore.Entry;
import fr.ul.sid.dicegame.highscore.connexionDatabase.ConnexionMySQL;

public class HighScoreMySQL implements HighScore {

	private Entry hs;

	private static Logger logger = Logger.getLogger(HighScoreMySQL.class.getName());

	@Override
	public void load() {
		Connection conn = ConnexionMySQL.getInstance();
		try {
			PreparedStatement statement = conn.prepareStatement("Select * from highscore where id = 1");
			ResultSet res = statement.executeQuery();
			if (res.next()) {
				String nom = res.getString("nom");
				int score = res.getInt("score");
				hs = new Entry(nom, score);
				logger.info("R�cup�ration r�ussi.");
			}
		} catch (SQLException e) {
			logger.severe(e.getMessage());
		}
	}

	@Override
	public void save() {
		Connection conn = ConnexionMySQL.getInstance();
		try {
			PreparedStatement statement = conn.prepareStatement("Select count(*) from highscore");
			ResultSet res = statement.executeQuery();
			if (res.next()) {
				if (res.getInt(1) > 0) {
					logger.info("Actualisation des informations dans la table");
					try {
						PreparedStatement statement2 = conn
								.prepareStatement("Update highscore set nom = ?, score = ? where id = 1");
						statement2.setString(1, hs.getNom());
						statement2.setInt(2, hs.getScore());
						int res2 = statement2.executeUpdate();
						if (res2 == 1) {
							logger.info("Enregistrement r�ussi.");
						}
					} catch (SQLException e) {
						logger.severe(e.getMessage());
					}
				} else {
					logger.info("Ajout des informations dans la table");
					try {
						PreparedStatement statement2 = conn
								.prepareStatement("insert into highscore (id, nom, score) values(1, ?, ?)");
						statement2.setString(1, hs.getNom());
						statement2.setInt(2, hs.getScore());
						int res2 = statement2.executeUpdate();
						if (res2 == 1) {
							logger.info("Enregistrement r�ussi.");
						}
					} catch (SQLException e) {
						logger.severe(e.getMessage());
					}
				}
			}
		} catch (SQLException e) {
			logger.severe(e.getMessage());
		}
	}

	@Override
	public String whoTheHellAreYou() {
		System.out.println("MySQL");
		return "MySQL";
	}

	@Override
	public void add(String nom, int score) {
		this.hs = new Entry(nom, score);
	}

	public Entry getHs() {
		return hs;
	}
}