package fr.ul.sid.dicegame.historique;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class CareTaker extends Observable {

	private List<Memento> mementoList = new ArrayList<Memento>();

	public void add(Memento state) {
		mementoList.add(state);
		setChanged();
		notifyObservers(this);
	}

	public Memento get(int index) {
		return mementoList.get(index);
	}

	public String toString() {
		String historique = "";

		for (int i = 0; i < mementoList.size(); i++) {
			historique += mementoList.get(i).getState() + "\n";
		}

		return historique;
	}
}