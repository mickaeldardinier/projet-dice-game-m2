package fr.ul.sid.dicegame.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import fr.ul.sid.dicegame.DiceGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

public class ControllerHighScore implements Initializable {

	@FXML
	private TextArea highScoreText;

	@FXML
	private Button rafraichir;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		rafraichir.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (DiceGame.getInstance().getHighscore().getHs() == null) {
					highScoreText.setText("Solution : " + DiceGame.getInstance().getHighscore().whoTheHellAreYou()
							+ "\nAucun Highscore enregistré");
				} else {
					highScoreText.setText("Solution : " + DiceGame.getInstance().getHighscore().whoTheHellAreYou()
							+ "\n" + DiceGame.getInstance().getHighscore().getHs().toString());
				}
			}
		});
	}
}
