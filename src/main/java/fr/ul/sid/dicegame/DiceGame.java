package fr.ul.sid.dicegame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.ul.sid.dicegame.highscore.kit.MariaDBKit;
import fr.ul.sid.dicegame.highscore.kit.MongoDBKit;
import fr.ul.sid.dicegame.highscore.kit.MySQLKit;
import fr.ul.sid.dicegame.highscore.kit.PersistKit;
import fr.ul.sid.dicegame.highscore.kit.SrKit;
import fr.ul.sid.dicegame.highscore.object.HighScore;
import fr.ul.sid.dicegame.historique.CareTaker;
import fr.ul.sid.dicegame.historique.Originator;
import fr.ul.sid.dicegame.observers.HistoriqueObserver;
import fr.ul.sid.dicegame.strategy.RollStrategy;
import javafx.scene.control.TextArea;

public class DiceGame {

	private static DiceGame instance;

	private HighScore highscore;

	private List<Player> players;

	private static final int NOMBRE_TOUR = 5;

	private int iterrateurPlayer;

	private Player currentPlayer;

	private int currentTour;

	private Player nextPlayer;

	private boolean finPartie;

	private Originator originator;
	private CareTaker careTaker;

	private DiceGame() {
		players = new ArrayList<Player>();
		iterrateurPlayer = 1;
		currentPlayer = null;
		nextPlayer = null;
		currentTour = 1;
		finPartie = false;

		originator = new Originator();
		careTaker = new CareTaker();
		careTaker.addObserver(HistoriqueObserver.getInstance());
		genererHighScore();
	}

	public static DiceGame getInstance() {
		if (instance == null) {
			instance = new DiceGame();
		}
		return instance;
	}

	public static void reset() {
		instance = new DiceGame();
	}

	public void genererHighScore() {
		PersistKit pk;
		int min = 1;
		int max = 40;
		Random random = new Random();
		int number = random.nextInt(max - min + 1) + min;
		if (number < 10) {
			pk = new MySQLKit();
		} else if (number < 20) {
			pk = new SrKit();
		} else if (number < 30) {
			pk = new MongoDBKit();
		} else {
			pk = new MariaDBKit();
		}
		highscore = pk.makeKit();
		highscore.whoTheHellAreYou();
		highscore.load();
	}

	public void majHighScore(Player p) {
		if (highscore.getHs() != null) {
			if (p.getScore() > highscore.getHs().getScore()) {
				highscore.add(p.getName(), p.getScore());
				highscore.save();
			}
		} else {
			highscore.add(p.getName(), p.getScore());
			highscore.save();
		}
	}
	
	public ArrayList<Integer> tourJoueur(Die d1, Die d2, Player p, RollStrategy strategie) {
		d1.roll(strategie);
		d2.roll(strategie);

		int resultat1 = d1.getFaceValue();
		int resultat2 = d2.getFaceValue();

		ArrayList<Integer> resultats = new ArrayList<Integer>();
		resultats.add(resultat1);
		resultats.add(resultat2);

		if (resultat1 + resultat2 == 7) {
			p.ajouterScore(10);
			resultats.add(10);
		} else if(resultat1 + resultat2 == 6) {
			p.ajouterScore(3);
			resultats.add(3);
		} else
			resultats.add(0);
		originator.setState("Le joueur " + p.getName() + " a choisi la " + strategie.toString()
				+ " et a obtenu le r�sultat " + resultats.get(0) + "/" + resultats.get(1) + ".\n Il a donc marqu� "
				+ resultats.get(2) + " points, son score s'�l�ve � " + p.getScore() + " points.\n");
		careTaker.add(originator.saveStateToMemento());

		return resultats;
	}

	public void gererTour() {
		if (currentTour > NOMBRE_TOUR && currentPlayer.equals(players.get(players.size() - 1))) {
			finPartie = true;
			originator.setState("---------Fin de la partie---------");
			careTaker.add(originator.saveStateToMemento());
			Classement.ordonnerClassement(players);
			majHighScore(players.get(0));
		}

		else {
			if (iterrateurPlayer == getPlayers().size()) {
				iterrateurPlayer = 0;
				if (currentTour <= NOMBRE_TOUR) {
					originator.setState("---Tour " + currentTour + "---\n");
					careTaker.add(originator.saveStateToMemento());
				}
			}

			if (iterrateurPlayer + 1 == getPlayers().size()) {
				currentTour++;
				nextPlayer = getPlayers().get(0);

			} else {
				nextPlayer = getPlayers().get(iterrateurPlayer + 1);
			}

			currentPlayer = getPlayers().get(iterrateurPlayer);
			iterrateurPlayer++;
		}
	}

	public HighScore getHighscore() {
		return highscore;
	}

	public void setHighscore(HighScore highscore) {
		this.highscore = highscore;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
		this.currentPlayer = this.players.get(0);
		if (this.players.size() > 1) {
			this.nextPlayer = this.players.get(1);
		} else
			this.nextPlayer = this.players.get(0);
		originator.setState("---Tour " + currentTour + "---\n");
		careTaker.add(originator.saveStateToMemento());
	}

	public boolean isFinPartie() {
		return finPartie;
	}

	public void setFinPartie(boolean finPartie) {
		this.finPartie = finPartie;
	}

	public Player getNextPlayer() {
		return nextPlayer;
	}

	public void setNextPlayer(Player nextPlayer) {
		this.nextPlayer = nextPlayer;
	}

	public int getIterrateurPlayer() {
		return iterrateurPlayer;
	}

	public void setIterrateurPlayer(int iterrateurPlayer) {
		this.iterrateurPlayer = iterrateurPlayer;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public int getCurrentTour() {
		return currentTour;
	}

	public void setCurrentTour(int currentTour) {
		this.currentTour = currentTour;
	}

	public static int getNombreTour() {
		return NOMBRE_TOUR;
	}

	public Originator getOriginator() {
		return originator;
	}

	public void setOriginator(Originator originator) {
		this.originator = originator;
	}

	public CareTaker getCareTaker() {
		return careTaker;
	}

	public void setCareTaker(CareTaker careTaker) {
		this.careTaker = careTaker;
	}

}
