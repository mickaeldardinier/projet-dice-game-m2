package fr.ul.sid.dicegame;

import fr.ul.sid.dicegame.strategy.RollStrategy;

public class Die {

	private int faceValue = 1;

	public Die() {

	}

	public void roll(RollStrategy rd) {
		int res = rd.roll();
		setFaceValue(res);
	}

	public int getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(int faceValue) {
		this.faceValue = faceValue;
	}
}
