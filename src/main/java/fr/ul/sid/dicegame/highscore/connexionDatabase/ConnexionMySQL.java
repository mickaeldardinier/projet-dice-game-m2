package fr.ul.sid.dicegame.highscore.connexionDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnexionMySQL {

	private static Connection connexion;

	private static final Logger logger = Logger.getLogger(ConnexionMySQL.class.getName());

	private ConnexionMySQL() {
		try {
			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/dicegame?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
			logger.info("Base de donn�es pr�te � l'emploi.");
		} catch (SQLException e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
	}

	// M�thode qui va nous retourner notre instance et la cr�er si elle n'existe
	// pas
	public static Connection getInstance() {
		if (connexion == null) {
			new ConnexionMySQL();
		}
		return connexion;
	}

}
