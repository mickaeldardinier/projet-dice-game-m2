package fr.ul.sid.dicegame.highscore;
import java.util.Random;

import fr.ul.sid.dicegame.highscore.kit.MariaDBKit;
import fr.ul.sid.dicegame.highscore.kit.MongoDBKit;
import fr.ul.sid.dicegame.highscore.kit.MySQLKit;
import fr.ul.sid.dicegame.highscore.kit.PersistKit;
import fr.ul.sid.dicegame.highscore.kit.SrKit;
import fr.ul.sid.dicegame.highscore.object.HighScore;

public class Main {

	public static void main(String[] args) {
		HighScore hs;
		PersistKit pk;
		
		int min = 1;
		int max = 30;
	
		for (int i = 0; i < 4; i++) {	
			Random random = new Random();
			int number = random.nextInt(max - min + 1) + min;
			if (number < 10) {
				pk = new MySQLKit();
			} else if(number < 20){
				pk = new SrKit();
			}else {
				pk = new MariaDBKit();
			}
			System.out.println("\nRandom " + i + " :");
			hs = pk.makeKit();
			hs.whoTheHellAreYou();
		}
		
		//Pour fichier
		System.out.println("Kit Serial");
		pk = new SrKit();
		hs = pk.makeKit();
		hs.add("Moi Fichier", 50);
		hs.save();
		
		pk = new SrKit();
		hs = pk.makeKit();
		hs.load();
		System.out.println(hs.getHs());
		
		//Pour Sql
		System.out.println("Kit JDBC");
		pk = new MySQLKit();
		hs = pk.makeKit();
		hs.add("Moi SQL", 50);
		hs.save();
		
		pk = new MySQLKit();
		hs = pk.makeKit();
		hs.load();
		System.out.println(hs.getHs());
		
		//Pour MariaDB
		System.out.println("Kit MariaDB");
		pk = new MariaDBKit();
		hs = pk.makeKit();
		hs.add("Moi MariaDB", 50);
		hs.save();
		
		pk = new MariaDBKit();
		hs = pk.makeKit();
		hs.load();
		System.out.println(hs.getHs());
		
		//Pour MongoDB
		System.out.println("Kit MongoDB");
		pk = new MongoDBKit();
		hs = pk.makeKit();
		hs.add("Moi NoSql 2", 90);
		hs.save();
		
		pk = new MongoDBKit();
		hs = pk.makeKit();
		hs.load();
		System.out.println(hs.getHs());
	}
}