package fr.ul.sid.dicegame.highscore.kit;

import fr.ul.sid.dicegame.highscore.object.HighScore;

public interface PersistKit {

	public HighScore makeKit();
}