package fr.ul.sid.dicegame.highscore.kit;

import fr.ul.sid.dicegame.highscore.object.HighScore;
import fr.ul.sid.dicegame.highscore.object.HighScoreMySQL;

public class MySQLKit implements PersistKit {

	@Override
	public HighScore makeKit() {
		return new HighScoreMySQL();
	}
}