package fr.ul.sid.dicegame.highscore.object;

import fr.ul.sid.dicegame.highscore.Entry;

public interface HighScore {

	public void add(String nom, int score);

	public void load();

	public void save();

	public String whoTheHellAreYou();
	
	public Entry getHs();
}