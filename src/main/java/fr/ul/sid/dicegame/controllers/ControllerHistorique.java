package fr.ul.sid.dicegame.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import fr.ul.sid.dicegame.observers.HistoriqueObserver;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

public class ControllerHistorique implements Initializable {

	@FXML
	private TextArea historique;
		
	private HistoriqueObserver observer = HistoriqueObserver.getInstance();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		observer.setHistorique(historique);
	}
}
