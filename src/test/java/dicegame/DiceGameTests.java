package dicegame;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import fr.ul.sid.dicegame.Classement;
import fr.ul.sid.dicegame.Player;

public class DiceGameTests {

	@Test
	public void testClassement1() {
		Player p1 = new Player("Sushi");
		p1.setScore(20);
		Player p2 = new Player("Trotro");
		p2.setScore(25);
		Player p3 = new Player("Bambi");
		p3.setScore(22);
		ArrayList<Player> players = new ArrayList<Player>();

		players.add(p1);
		players.add(p2);
		players.add(p3);

		HashMap<Player, Integer> classement = Classement.ordonnerClassement(players);
		assertEquals(new Integer(3), classement.get(p1));
		assertEquals(new Integer(1), classement.get(p2));
		assertEquals(new Integer(2), classement.get(p3));
	}
	
	@Test
	public void testClassement2() {
		Player p1 = new Player("Sushi");
		p1.setScore(23);
		Player p2 = new Player("Trotro");
		p2.setScore(25);
		Player p3 = new Player("Bambi");
		p3.setScore(22);
		Player p4 = new Player("Titi");
		p4.setScore(25);
		ArrayList<Player> players = new ArrayList<Player>();

		players.add(p1);
		players.add(p2);
		players.add(p3);
		players.add(p4);
		
		HashMap<Player, Integer> classement = Classement.ordonnerClassement(players);
		assertEquals(new Integer(2), classement.get(p1));
		assertEquals(new Integer(1), classement.get(p2));
		assertEquals(new Integer(3), classement.get(p3));
		assertEquals(new Integer(1), classement.get(p4));
	}
	
	@Test
	public void testClassement3() {
		Player p1 = new Player("Sushi");
		p1.setScore(20);
		Player p2 = new Player("Trotro");
		p2.setScore(20);
		Player p3 = new Player("Bambi");
		p3.setScore(20);
		Player p4 = new Player("Titi");
		p4.setScore(20);
		ArrayList<Player> players = new ArrayList<Player>();

		players.add(p1);
		players.add(p2);
		players.add(p3);
		players.add(p4);
		
		HashMap<Player, Integer> classement = Classement.ordonnerClassement(players);
		assertEquals(new Integer(1), classement.get(p1));
		assertEquals(new Integer(1), classement.get(p2));
		assertEquals(new Integer(1), classement.get(p3));
		assertEquals(new Integer(1), classement.get(p4));
	}
}
